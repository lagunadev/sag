<?php 
    class PageModel{
        public function sectionModel($enlaceModel){
            if ($enlaceModel == "cIndividual" ||
            $enlaceModel == "embarque" ||
            $enlaceModel == "aCorrales" ||
            $enlaceModel == "aLocalidades" ||
            $enlaceModel == "aProveedores") {

                $seccion = "views/sections/".$enlaceModel.".php";
                return $seccion;
            }else{
                    $seccion = "views/sections/dashboard.php";
                    return $seccion;
            }
        }
    }
