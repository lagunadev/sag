<!-- *******************************************TERMINA MENU*************************************** -->
            <div id="page-wrapper">
                <div id="home" class="tab-pane fade in active">
                    
                        <div class="row">
                            <div class="col-md-12">
                                <h3>Listado de Corrales</h3>
                                <button class="btn btn-primary" data-toggle="modal" data-target="#modalnvocorral">Nuevo Corral</button>
                            </div>  
                        </div><br />
                  
                
                
                <?php
                    //include '../back_sistema/config/conexion.php';
                      $sql ="select * from corral";
                      $result = $con->query($sql);
                      if ($result->num_rows > 0) 
                      {
                        echo "<table class='table table-striped'>
                                <tr>
                                  <th>Numero de Corral</th>
                                  <th>Capacidad (# Cabezas)</th>
                                  <th>Acciones</th>
                                </tr>
                                <tr>";
                                  while($row = $result->fetch_assoc()) 
                                  {
                                    echo  "<td>" . $row["id_corral"].            "</td>".
                                          "<td>" . $row["capacidad"].        "</td>".
                                          "<td>
                                            &nbsp&nbsp 
                                            <a  data-id_corral='$row[id_corral]' 
                                                data-capacidad='$row[capacidad]' 
                                                
                                                data-toggle='modal' 
                                                data-target='#updatecorral' 
                                                class='glyphicon glyphicon-pencil'>
                                            </a>
                                            </a> &nbsp".
                                            "<a href='admin_php/eliminar_corral.php?id_corral=$row[id_corral]' class='glyphicon glyphicon-trash'> 
                                            </a>
                                          </td>
                                        </tr>";
                                  }
                      } else {
                          echo "0 results";
                      }
                     ?>
                              </table>
              </div>


            <!-- MODAL para Nuevo Corral -->
             <div class="modal fade" id="modalnvocorral" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Nuevo Corral</h4>
                        </div>
                        <div class="modal-body">
                            <div id="datos_ajax"></div>
                            <form action="admin_php/add_corral.php" method="GET">
                                <div class="form-group">
                                    <label for="id" class="control-label">Numero de Corral:</label>
                                    <input  type="text" class="form-control" id="id_corral" name="id_corral">
                                </div>
                                <div class="form-group">
                                    <label for="nombre" class="control-label">Capacidad:</label>
                                    <input type="text" class="form-control" id="capacidad" name="capacidad">
                                </div>
                            
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                <button type="submit" class="btn btn-primary">Guardar</button>
                            </form>
                        </div>
                    <div class="modal-footer">
                      
                    </div>
                  </div>
                </div>
              </div>

              <!-- MODAL para actualizar Corral -->
             <div class="modal fade" id="updatecorral" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Actualizar Datos de Corral</h4>
                        </div>
                        <div class="modal-body">
                            <div id="datos_ajax"></div>
                            <form action="admin_php/actualizar_corral.php" method="GET">
                                <div class="form-group">
                                    <label for="id_corral" class="control-label">Numero de Corral:</label>
                                    <input  type="text" class="form-control" id="id_corral" name="id_corral">
                                </div>
                                <div class="form-group">
                                    <label for="capacidad" class="control-label">Capacidad:</label>
                                    <input type="text" class="form-control" id="capacidad" name="capacidad">
                                </div>
                            
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                <button type="submit" class="btn btn-primary">Guardar</button>
                            </form>
                        </div>
                    <div class="modal-footer">
                      
                    </div>
                  </div>
                </div>
              </div>

            </div>  <!-- /#page-wrapper -->

                 <!-- /.panel-heading -->
                       
        </div>
        <!-- /#page-wrapper -->