<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <p class="text"><label>Compra Individual</label></p>                
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <form role="form">
    
        <div class="row">
            <div class="col-lg-12">
    
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Datos del Proveedor
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
    
                                <div class="col-sm-6">   
                                    <label>Proveedor:</label>
                                    <div class="form-group">
                                        <div class="selector-proveedor">
                                            <select class="form-control"></select><!-- i nvoca a ajax/autocomplete/proveedor.php-->
                                        </div>
                                    </div>
                                </div>
    
    
                                <div class="col-sm-6">   
                                    <label>Origen:</label>
                                    <div class="form-group">
                                        <div class="selector-localidad">
                                            <select class="form-control"></select><!-- invoca a ajax/autocomplete/proveedor.php-->
                                        </div>
                                    </div>
                                </div>
    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Grupo de pesaje
                    </div>
                    <div class="panel-body">
    
                        <div class="row">
                            <div class="col-lg-12">
    
                                <div class="col-sm-3"> 
                                    <div class="form-group">
                                        <label>Arete:</label>
                                        <input class="form-control" placeholder="Arete">
                                    </div>
                                </div>
    
                                <div class="col-sm-3">   
                                    <label>Clasificación:</label>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option>Seleccione..</option>
                                            <option>Engorda</option>
                                        </select>
                                    </div>
                                </div>
    
                                <div class="col-sm-3">   
                                    <label>Tratamiento:</label>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option>Seleccione..</option>
                                            <option>opcion 1</option>
                                            <option>opcion 2</option>
                                        </select>
                                    </div>
                                </div>
    
                                <div class="col-sm-3">   
                                    <label>Corral:</label>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option>Seleccione..</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </div>
                                </div>
    
                            </div>
                        </div>
    
                        <div class="row">
                            <div class="col-lg-12">
    
    
                                <div class="col-sm-3">   
                                    <label>Sexo:</label>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>Hembra
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>Macho
                                        </label>
                                    </div>
                                </div>
    
    
                                <div class="col-sm-3"> 
                                    <div class="form-group">
                                        <label>Peso:</label>
                                        <input class="form-control" placeholder="Peso en KG completos">
                                    </div>
                                </div>
    
                                <div class="col-sm-3"> 
                                    <div class="form-group">
                                        <label>Precio:</label>
                                        <input class="form-control" placeholder="00.00">
                                    </div>
                                </div>
    
                                <div class="col-sm-3">   
                                    <label>Tipo Costo:</label>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option>Seleccione..</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
    
                        <div align="right">
                            <button class="btn btn-primary" type="button">Agregar</button>
                            <button class="btn btn-primary" type="button">Importar</button>
                        </div>
    
    
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel-heading">  
                                    <strong>Detalles:</strong>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Arete</th>
                                                    <th>Corral</th>
                                                    <th>Peso Compra (Kg)</th>
                                                    <th>Peso Venta (Kg)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>123465798</td>
                                                    <td>7</td>
                                                    <td>250</td>
                                                    <td>300</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
    
    
    
                        <div class="row">
                            <div class="panel-default">
                                <div class="col-lg-6">
                                    <p class="text"><label>Total de Cabezas: </label> 10 <label>Total de Kilogramos: </label> 5000</p>
                                </div>
                                <div class="col-lg-6">
                                    <div align="right">
                                        <button class="btn btn-primary" type="button">Guardar</button>
                                        <button class="btn btn-primary" type="button">Cancelar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
    
    
    
                    </div>
                </div>
            </div>
        </div>
    </form>
    
</div> 