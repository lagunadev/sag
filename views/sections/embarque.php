<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <p class="text-right"><label>Fecha:</label><?php    echo " ".date("j, n, Y"); ?></p>                
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Datos de Embarque
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form role="form">
                                <div class="col-sm-6">   
                                    <div class="form-group">
                                        <label>Folio:</label>
                                        <p class="form-control-static">001</p>
                                    </div>
                                </div>
    
    
                                <div class="col-sm-6">   
                                    <label>Destino:</label>
                                    <div class="form-group input-group">
                                        <input type="text" class="form-control">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Detalle de Embarque
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form role="form">
                                <label>Arete:</label>
                                <div class="form-group input-group">
                                    <input type="text" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                                <button class="btn btn-primary" type="button">Agregar</button>
                                <button class="btn btn-primary" type="button">Importar</button>
                        </div>
                        <div class="col-lg-12">
                            <div class="panel-heading">  
                                Detalles
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Arete</th>
                                                <th>Corral</th>
                                                <th>Peso Compra (Kg)</th>
                                                <th>Peso Venta (Kg)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>123465798</td>
                                                <td>7</td>
                                                <td>250</td>
                                                <td>300</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="panel-default">
                                    <div class="col-lg-6">
                                        <p class="text"><label>Total de Cabezas: </label> 10 <label>Total de Kilogramos: </label> 5000</p>
                                    </div>
                                    <div class="col-lg-6">
                                        <button class="btn btn-primary" type="button">Guardar</button>
                                        <button class="btn btn-primary" type="button">Cancelar</button>
                                    </div>
    
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- /#page-wrapper -->