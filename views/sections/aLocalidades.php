<!-- *******************************************TERMINA MENU*************************************** -->
            <div id="page-wrapper">
                <div id="home" class="tab-pane fade in active">
                    
                        <div class="row">
                            <div class="col-md-12">
                                <h3>Listado de Localidades</h3>
                                <button class="btn btn-primary" data-toggle="modal" data-target="#modalnvalocalidad">Nueva Localidad</button>
                            </div>  
                        </div><br />
                  
                
                
                <?php
                    //include '../back_sistema/config/conexion.php';
                      $sql ="select * from localidad";
                      $result = $con->query($sql);
                      if ($result->num_rows > 0) 
                      {
                        echo "<table class='table table-striped'>
                                <tr>
                                  <th>No. Localidad</th>
                                  <th>Nombre de Localidad</th>
                                  <th>Acciones</th>
                                </tr>
                                <tr>";
                                  while($row = $result->fetch_assoc()) 
                                  {
                                    echo  "<td>" . $row["id_localidad"].            "</td>".
                                          "<td>" . $row["nombre"].        "</td>".
                                          "<td>
                                            &nbsp&nbsp 
                                            <a  data-id_localidad='$row[id_localidad]' 
                                                data-nombre='$row[nombre]' 
                                                
                                                data-toggle='modal' 
                                                data-target='#updatelocalidad' 
                                                class='glyphicon glyphicon-pencil'>
                                            </a>
                                            </a> &nbsp".
                                            "<a href='admin_php/eliminar_localidad.php?id_localidad=$row[id_localidad]' class='glyphicon glyphicon-trash'> 
                                            </a>
                                          </td>
                                        </tr>";
                                  }
                      } else {
                          echo "0 results";
                      }
                     ?>
                              </table>
              </div>


            <!-- MODAL para Nueva localidad-->
             <div class="modal fade" id="modalnvalocalidad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Nueva Localidad</h4>
                        </div>
                        <div class="modal-body">
                            <div id="datos_ajax"></div>
                            <form action="admin_php/add_localidad.php" method="GET">
                                <div class="form-group">
                                    <label for="id" class="control-label">No. de Localidad:</label>
                                    <input  type="text" class="form-control" id="id_localidad" name="id_localidad" readonly="readonly">
                                </div>
                                <div class="form-group">
                                    <label for="nombre" class="control-label">Nombre:</label>
                                    <input type="text" class="form-control" id="nombre" name="nombre">
                                </div>
                            
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                <button type="submit" class="btn btn-primary">Guardar</button>
                            </form>
                        </div>
                    <div class="modal-footer">
                      
                    </div>
                  </div>
                </div>
              </div>

              <!-- MODAL para actualizar Localidad -->
             <div class="modal fade" id="updatelocalidad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Actualizar Datos de Localidad</h4>
                        </div>
                        <div class="modal-body">
                            <div id="datos_ajax"></div>
                            <form action="admin_php/actualizar_localidad.php" method="GET">
                                <div class="form-group">
                                    <label for="id_localidad" class="control-label">No. Localidad:</label>
                                    <input  type="text" class="form-control" id="id_localidad" name="id_localidad" readonly="readonly">
                                </div>
                                <div class="form-group">
                                    <label for="nombre" class="control-label">Nombre:</label>
                                    <input type="text" class="form-control" id="nombre" name="nombre">
                                </div>
                            
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                <button type="submit" class="btn btn-primary">Guardar</button>
                            </form>
                        </div>
                    <div class="modal-footer">
                      
                    </div>
                  </div>
                </div>
              </div>

            </div>  <!-- /#page-wrapper -->

               