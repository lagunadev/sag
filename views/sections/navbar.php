<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.html">Sistema de Administración de Ganado</a>
    </div>
    <!-- /.navbar-header -->
    
    <ul class="nav navbar-top-links navbar-right">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-messages">
                <li>
                    <a href="#">
                        <div>
                            <strong>praaa</strong>
                            <span class="pull-right text-muted">
                                <em>dia</em>
                            </span>
                        </div>
                        <div>vida...</div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <strong>praaa</strong>
                            <span class="pull-right text-muted">
                                <em>vida....</em>
                            </span>
                        </div>
                        <div>vida...</div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <strong>John Smith</strong>
                            <span class="pull-right text-muted">
                                <em>Yesterday</em>
                            </span>
                        </div>
                        <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a class="text-center" href="#">
                        <strong>Leer todos los mensajes</strong>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </li>
            </ul>
            <!-- /.dropdown-messages -->
        </li>
        
        
        
        <!-- /.dropdown -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="#"><i class="fa fa-user fa-fw"></i> Administrador</a>
                </li>
                <li><a href="#"><i class="fa fa-gear fa-fw"></i> Configuración</a>
                </li>
                <li class="divider"></li>
                <li><a href="../back_sistema/config/logout.php"><i class="fa fa-sign-out fa-fw"></i> Salir</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->
    
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li class="sidebar-search">
                    <div class="input-group custom-search-form">
                        <input type="text" class="form-control" placeholder="Search...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                </li>
                <li>
                    <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Tablero SAG</a>
                </li>
                <li>
                    <a href="ganado.php"><i class="fa fa-edit fa-fw"></i> Ganado<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?section=cIndividual">Compra Individual</a>
                        </li>
                        <li>
                            <a href="index.php?section=embarque">Embarque</a>
                        </li>
                        <li>
                            <a href="salida_por_muerte.php">Salida por Muerte</a>
                        </li>
                        <li>
                            <a href="salida_por_venta.php">Salida por Venta</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="inventario.php"><i class="fa fa-bar-chart-o fa-fw"></i> Inventario<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="compras.php">Compras</a>
                        </li>
                        <li>
                            <a href="inventario.php">Inventario Teórico <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li>
                                    <a href="animales.php">Animales</a>
                                </li>
                                <li>
                                    <a href="productos.php">Productos</a>
                                </li>
                            </ul>
                            <!-- /.nav-third-level -->
                        </li>
                        <li>
                            <a href="transferencia_de_producto.php">Transferencia de Producto</a>
                        </li>                                
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="mante.php"><i class="fa fa-gear fa-fw"></i> Mantenimiento<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="alimentacion_corral.php">Alimentación Corral</a>
                        </li>
                        <li>
                            <a href="aplicar_tratamiento.php">Aplicar Tratamientos</a>
                        </li>
                        <li>
                            <a href="transferir_animales.php">Transferir Animales</a>
                        </li>
                        <li>
                            <a href="transferir_corral.php">Transferir Corral</a>
                        </li>
                        <li>
                            <a href="ver_animales.php">Ver Animales</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="#"><i class="fa fa-table fa-fw"></i> Otros<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="gasto.php">Gastos</a>
                        </li>
                        <li>
                            <a href="chequeras.php">Chequeras</a>
                        </li>
                        <li>
                            <a href="movimientos.php">Movimientos</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="#"><i class="fa fa-table fa-fw"></i> Administrar<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="index.php?section=aCorrales">Corrales</a>
                        </li>
                        <li>
                            <a href="index.php?section=aLocalidades">Localidades</a>
                        </li>
                        <li>
                            <a href="index.php?section=aProveedores">Provedores</a>
                        </li>
                        <li>
                            <a href="aprecio.php">Precios</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>
