<?php 
    class PageController{
        //metodo para cargar la plantilla (VISTA) de mi app
        public function template(){
                include 'views/template.php';
        }

        /*metodo para cargar las secciones en funcion de lo que el usuario
        seleccion en el menu*/
        public function sectionController(){
                if (isset($_GET["section"])) {
                        $sectionController = $_GET["section"];
                }else{
                        $sectionController = "dashboard";
                }

                $section = PageModel::sectionModel($sectionController);
                include $section;
        }
    }
